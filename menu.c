#include <assert.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "2D.h"
#include "text_image.h"
#include "screens.h"
#include "assets.h"

enum {
  TOP_TITLE_OFFSET = 50,
  TOP_MENU_OFFSET = 250
};

static struct Dim2D screen_dim;
static struct TextImage title, new_game, scores;
static SDL_Renderer *g_rend;

static void destroy(void) {
  DestroyTextImage(&title);
  DestroyTextImage(&new_game);
  DestroyTextImage(&scores);
}

static int HandleEvent(const SDL_Event *e) {
  if (e->type == SDL_MOUSEBUTTONDOWN) {
    int x, y;
    SDL_GetMouseState(&x, &y);
    if (InBounds(&new_game, x, y)) {
      ChangeScreen(GAME_SCREEN);
    }
    else if (InBounds(&scores, x, y)) {
      ChangeScreen(SCORES_SCREEN);
    }
  }
  return 0;
}

static int Update(void) {
  return 0;
}

static int Focus(void) {
  return 0;
}

static int Render(void) {
  RenderTextImage(&title);
  RenderTextImage(&new_game);
  RenderTextImage(&scores);
  return 0;
}

int InitMenu(SDL_Renderer *g_rend_, const PixelDim2D *screen_dim_) {
  g_rend = g_rend_;
  screen_dim = *screen_dim_;

  TTF_Font *title_font = GetLargeFont();
  TTF_Font *button_font = GetMediumFont();

  InitTextImage(&title, title_font, "Tetris", g_rend, &DEFAULT_FG_COLOR);
  InitTextImage(&new_game, button_font, "New Game", g_rend, &DEFAULT_FG_COLOR);
  InitTextImage(&scores, button_font, "High Scores", g_rend, &DEFAULT_FG_COLOR);

  title.pos.x = HorizontalCenterWithin(&title.dim, &screen_dim);
  title.pos.y = TOP_TITLE_OFFSET;
  new_game.pos.x = HorizontalCenterWithin(&new_game.dim, &screen_dim);
  new_game.pos.y = TOP_MENU_OFFSET;
  scores.pos.x = HorizontalCenterWithin(&scores.dim, &screen_dim);
  scores.pos.y = TOP_MENU_OFFSET + new_game.dim.h;

  const struct ScreenObject self = {
    .destroy = destroy,
    .HandleEvent = HandleEvent,
    .Update = Update,
    .Render = Render,
    .Focus = Focus
  };
  RegisterScreen(MENU_SCREEN, &self);

  return 0;
}
