#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>

#include "xSDL.h"
#include "assets.h"

static const char *FONT_FILE = "arcade.ttf";
static const char *TETRIS_BLOCK_FILE = "tetris_block.png";
static const char *BG_IMG_FILE = "bg.png";

static TTF_Font *large_font, *medium_font, *small_font;
static SDL_Texture *tetris_block, *bg_img;

#define ASSERT_VALID_ASSETS() \
  do { \
    SDL_assert(large_font); \
    SDL_assert(medium_font); \
    SDL_assert(small_font); \
    SDL_assert(tetris_block); \
    SDL_assert(bg_img); \
  } while (0)

#define ASSERT_NULL_ASSETS() \
  do { \
    SDL_assert(!large_font); \
    SDL_assert(!medium_font); \
    SDL_assert(!small_font); \
    SDL_assert(!tetris_block); \
    SDL_assert(!bg_img); \
  } while (0)

int InitAssets(SDL_Renderer *r) {
  ASSERT_NULL_ASSETS();

  large_font = TTF_OpenFont(FONT_FILE, LARGE_FONT_SIZE);

  medium_font = TTF_OpenFont(FONT_FILE, MEDIUM_FONT_SIZE);

  small_font = TTF_OpenFont(FONT_FILE, SMALL_FONT_SIZE);

  SDL_Surface *aux_img = IMG_Load(TETRIS_BLOCK_FILE);

  tetris_block = SDL_CreateTextureFromSurface(r, aux_img);
  SDL_FreeSurface(aux_img);


  aux_img = IMG_Load(BG_IMG_FILE);

  bg_img = SDL_CreateTextureFromSurface(r, aux_img);
  SDL_FreeSurface(aux_img);

  ASSERT_VALID_ASSETS();

  return 0;
}

TTF_Font* GetLargeFont(void) {
  ASSERT_VALID_ASSETS();
  return large_font;
}

TTF_Font* GetMediumFont(void) {
  ASSERT_VALID_ASSETS();
  return medium_font;
}

TTF_Font* GetSmallFont(void) {
  ASSERT_VALID_ASSETS();
  return small_font;
}

SDL_Texture* GetTetrisBlockImg(void) {
  ASSERT_VALID_ASSETS();
  return tetris_block;
}

SDL_Texture* GetBgImg(void) {
  ASSERT_VALID_ASSETS();
  return bg_img;
}

void DestroyAssets(void) {
  xSDL_DestroyTexture(&bg_img);
  xSDL_DestroyTexture(&tetris_block);
  xTTF_CloseFont(&small_font);
  xTTF_CloseFont(&medium_font);
  xTTF_CloseFont(&large_font);
}
