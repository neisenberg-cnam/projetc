#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "xSDL.h"
#include "assets.h"
#include "text_image.h"

int RenderTextImage(struct TextImage *ti) {
  SDL_Rect region = {
    .x = ti->pos.x, .y = ti->pos.y,
    .w = ti->dim.w, .h = ti->dim.h
  };
  SDL_RenderCopy(ti->rend, ti->image, 0, &region);
  return 0;
}

int InitTextImage(struct TextImage *ti,
                TTF_Font *font,
                const char *text,
                SDL_Renderer *rend,
                const SDL_Color *color)
{
  ti->image = 0;
  SDL_Surface *stext = TTF_RenderText_Solid(font, text, *color);
  ti->image = SDL_CreateTextureFromSurface(rend, stext);
  ti->dim = (PixelDim2D) {stext->w, stext->h};
  ti->pos = (PixelPoint2D) {0, 0};
  SDL_FreeSurface(stext);
  ti->rend = rend;
  return 0;
}

int InBounds(const struct TextImage *ti, int x, int y) {
  return x >= ti->pos.x
    && x <= (ti->pos.x + ti->dim.w)
    && y >= ti->pos.y
    && y <= (ti->pos.y + ti->dim.h);
}

void DestroyTextImage(struct TextImage *ti) {
  xSDL_DestroyTexture(&ti->image);
}
