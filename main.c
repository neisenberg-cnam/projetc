#include <stdio.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>

#include "2D.h"
#include "assets.h"
#include "text_image.h"
#include "screens.h"
#include "menu.h"
#include "game.h"
#include "scores.h"

#include "xSDL.h"

enum {
  WIN_WIDTH = 540,
  WIN_HEIGHT = 640
};

static const char *WIN_TITLE = "Tetris";

static SDL_Window *window;
static SDL_Renderer *rend;
static PixelDim2D screen_size;
static struct ScreenObject all_screens[NUM_SCREENS];
static struct ScreenObject *current;

static int InitVideo(void) {
  window = SDL_CreateWindow(WIN_TITLE, SDL_WINDOWPOS_UNDEFINED,
    SDL_WINDOWPOS_UNDEFINED, WIN_WIDTH, WIN_HEIGHT, SDL_WINDOW_SHOWN);

  rend = SDL_CreateRenderer(window, -1,
    SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

  return 0;
}

static int InitScreens(void) {
  screen_size.w = WIN_WIDTH;
  screen_size.h = WIN_HEIGHT;

  InitMenu(rend, &screen_size);
  InitGame(rend, &screen_size);
  InitScores(rend, &screen_size);
  current = all_screens + MENU_SCREEN;
  return 0;
}

static int Init(void) {
  SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);
  InitVideo();
  TTF_Init();
  IMG_Init(IMG_INIT_PNG);
  InitAssets(rend);
  InitScreens();

  return 0;
}

static int GameLoop(void) {
  SDL_assert(current);
  current->Focus();
  SDL_Texture *bg = GetBgImg();

  for (;;) {
    SDL_Event e;
    while (SDL_PollEvent(&e)) {
      if (e.type == SDL_QUIT) {
        return 0;
      }
      current->HandleEvent(&e);
    }
    current->Update();
    SDL_RenderCopy(rend, bg, 0, 0);
    current->Render();
    SDL_RenderPresent(rend);
  }

  return 0;
}

static const char * FmtMessage(const char *msg, const char *alt) {
  return msg ? (*msg ? msg : alt) : alt;
}

static void Cleanup(void) {
  for (int i = 0; i < NUM_SCREENS; i++) {
    if (all_screens[i].destroy) {
      all_screens[i].destroy();
    }
  }
  xSDL_DestroyRenderer(&rend);
  xSDL_DestroyWindow(&window);
  TTF_Quit();
  IMG_Quit();
  SDL_Quit();
}

void RegisterScreen(const enum ScreenId which,
                const struct ScreenObject *screen) {
  const int screen_i = (int) which;
  SDL_assert(screen_i >= 0);
  SDL_assert(screen_i < NUM_SCREENS);
  all_screens[screen_i] = *screen;
}

void ChangeScreen(const enum ScreenId which) {
  const int screen_i = (int) which;
  SDL_assert(screen_i >= 0);
  SDL_assert(screen_i < NUM_SCREENS);
  current = all_screens + screen_i;
  current->Focus();
  return;
}

int Main(int argc, char *argv[]) {
  (void) argc;
  (void) argv;

  Init();
  GameLoop();
  Cleanup();
  return 0;
}
