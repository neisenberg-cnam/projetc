#ifndef _2D_H
#define _2D_H

struct Dim2D {
  int w, h;
};

struct Point2D {
  int x, y;
};

typedef struct Point2D Point2D;
typedef struct Dim2D Dim2D;

typedef Point2D PixelPoint2D;
typedef Dim2D PixelDim2D;

/**
 * Calcule la position x dans laquelle *thing devrait être pour pouvoir être
 * rendered au centre (horizontal) de *within.
 */
inline int HorizontalCenterWithin(const Dim2D *thing, const Dim2D *within)
{
  return (within->w - thing->w)/2;
}

#endif
