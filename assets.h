#ifndef ASSETS_H
#define ASSETS_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

enum {
  LARGE_FONT_SIZE = 46,
  MEDIUM_FONT_SIZE = 34,
  SMALL_FONT_SIZE = 22,
  BLOCK_WIDTH = 30,
  BLOCK_HEIGHT = 30,
  NUM_SONGS = 5
};

int InitAssets(SDL_Renderer *r);

TTF_Font* GetLargeFont(void);

TTF_Font* GetMediumFont(void);

TTF_Font* GetSmallFont(void);

void DestroyAssets(void);

SDL_Texture* GetTetrisBlockImg(void);

SDL_Texture* GetBgImg(void);

#endif
