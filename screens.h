#ifndef SCREENS_H
#define SCREENS_H

#include <SDL2/SDL.h>

#include "2D.h"

enum ScreenId {
  MENU_SCREEN, GAME_SCREEN, SCORES_SCREEN
};

static const struct SDL_Color DEFAULT_FG_COLOR = {225, 225, 225, 255};

enum {
  NUM_SCREENS = 3
};

struct GameContext {
  SDL_Renderer *rend;
  PixelDim2D dim;
};

typedef void (*ScreenDestroyFn)(void);
typedef int (*ScreenHandleEventFn)(const SDL_Event *e);
typedef int (*ScreenUpdateFn)(void);
typedef int (*ScreenRenderFn)(void);
typedef int (*ScreenFocusFn)(void);

struct ScreenObject {
  ScreenDestroyFn destroy;
  ScreenHandleEventFn HandleEvent;
  ScreenUpdateFn Update;
  ScreenRenderFn Render;
  ScreenFocusFn Focus;
};

/**
 * Les données de ScreenObject seront copiées en interne par l'implémentation
 * de la fonction RegisterScreen.
 */
void RegisterScreen(const enum ScreenId which,
                const struct ScreenObject *screen);

void ChangeScreen(const enum ScreenId which);

#endif
