#include <assert.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "xSDL.h"
#include "2D.h"
#include "text_image.h"
#include "screens.h"
#include "game.h"
#include "assets.h"
#include "scores.h"

#define WHITE_INIT_CODE {255, 255, 255, 255}
#define BLACK_INIT_CODE {0,   0,   0,   255}

#define COLOR0_INIT_CODE {255, 0,   0,   255}
#define COLOR1_INIT_CODE {0,   255, 0,   255}
#define COLOR2_INIT_CODE {0,   0,   255, 255}
#define COLOR3_INIT_CODE {255, 255, 0,   255}
#define COLOR4_INIT_CODE {255, 0,   255, 255}
#define COLOR5_INIT_CODE {0,   255, 255, 255}
#define COLOR6_INIT_CODE {0,   128, 255, 255}

static const SDL_Color BLACK = BLACK_INIT_CODE;
static const SDL_Color WHITE = WHITE_INIT_CODE;
static const SDL_Color PANEL_BORDER_COLOR = WHITE_INIT_CODE;

typedef struct Point2D GridPoint2D;
typedef struct Dim2D GridDim2D;

enum {
  PANEL_ROWS = 20,
  PANEL_COLS = 10,
  PADDING_PX = 30,

  // Une piece est toujours constituée de 4 blocs
  NUM_PIECE_PARTS = 4,

  // Délai de chute initial
  FALL_DELAY_MS = 300,

  // Au plus 1 touche peut être appuyée tous les KEY_PRESS_DELAY.
  KEY_PRESS_DELAY = 150,

  // Le nombre de type de pièce.
  NUM_DIFFERENT_PIECES = 7
};

struct FallingPiece {
  GridPoint2D blocks[NUM_PIECE_PARTS];
  GridPoint2D relative; // 0,0 correspond au coin en bas à gauche
  SDL_Color color;
};

struct Score {
  // label_text doit contenir "Score".
  // points_text doit contenir une représentation numérique du nombre de points que le joueurs possède.
  struct TextImage label_text, points_text;
  int points;
};

struct Panel {
  PixelDim2D block_dim;
  SDL_Rect geom;

  /**
   * Les blocs vides sont noirs.
   *
   * La ligne 0 correspond à la ligne du bas. La colonne 0 correspond à la colonne de gauche.
   * L'index des lignes progresse du bas vers le haut, et celle des colonnes de la gauche vers la droite.
   */
  SDL_Color blocks[PANEL_ROWS][PANEL_COLS];

  struct FallingPiece falling_piece, next_piece;
};

static const SDL_Color colors[NUM_DIFFERENT_PIECES] = {
  COLOR0_INIT_CODE, COLOR1_INIT_CODE, COLOR2_INIT_CODE, COLOR3_INIT_CODE,
  COLOR4_INIT_CODE, COLOR5_INIT_CODE, COLOR6_INIT_CODE

};

struct PieceTemplate {
  GridPoint2D fills[NUM_PIECE_PARTS];
  GridDim2D size;
};

static const struct PieceTemplate
template[NUM_DIFFERENT_PIECES] = {
  { .fills = { {0, 0}, {1, 0}, {2, 0}, {3, 0} },
    .size = {4, 1} },

  { .fills = { {0, 0}, {0, 1}, {1, 0}, {1, 1} },
    .size = {2, 2} },

  { .fills = { {0, 0}, {1, 0}, {1, 1}, {2, 1} },
    .size = {2, 1} },

  { .fills = { {0, 1}, {1, 1}, {1, 0}, {2, 0} },
    .size = {3, 1} },

  { .fills = { {0, 0}, {1, 0}, {2, 0}, {2, 1} },
    .size = {3, 2} },

  { .fills = { {0, 1}, {1, 1}, {2, 1}, {2, 0} },
    .size = {3, 2} },

  { .fills = { {0, 0}, {1, 0}, {2, 0}, {1, 1} },
    .size = {3, 2} }
};

static struct Panel panel;
static struct Score score;
static Uint32 last_update_ms;
static SDL_Texture *block;
static SDL_Renderer *g_rend;
static PixelDim2D screen_dim;

static void destroy(void) {
  DestroyTextImage(&score.label_text);
  DestroyTextImage(&score.points_text);
}

static int PanelHasBlock(int x, int y) {
  return !xSDL_ColorEq(panel.blocks[y] + x, &BLACK);
}

static int IsColliding(void) {
  const GridPoint2D *rel = &panel.falling_piece.relative;
  const GridPoint2D *blocks = panel.falling_piece.blocks;

  for (int i = 0; i < NUM_PIECE_PARTS; i++) {
    int x = rel->x + blocks[i].x;
    int y = rel->y + blocks[i].y;
    if (x < 0 || x >= PANEL_COLS || y < 0 ||
        (y < PANEL_ROWS && PanelHasBlock(x, y)))
    {
      return 1;
    }
  }
  return 0;
}

static void Flip(struct FallingPiece *piece) {
  GridPoint2D *blocks = piece->blocks;
  int adj_x = 0;
  int adj_y = 0;

  // Fait une rotation de pi/2 radians.
  for (int i = 0; i < NUM_PIECE_PARTS; i++) {
    int x = blocks[i].x;
    int y = blocks[i].y;
    int xp = -y;
    int yp = x;
    blocks[i].x = xp;
    blocks[i].y = yp;

    // Rassemblement des valeurs négatives.
    if (yp < 0 && -yp > adj_y) {
      adj_y = -yp;
    }
    if (xp < 0 && -xp > adj_x) {
      adj_x = -xp;
    }
  }

  // Normalisation des valeurs négatives rassemblées afin de ne plus en avoir dans
  // le résultat final.
  for (int i = 0; i < NUM_PIECE_PARTS; i++) {
    blocks[i].x += adj_x;
    blocks[i].y += adj_y;
  }
}

static void FlipBack(struct FallingPiece *piece) {
  Flip(piece);
  Flip(piece);
  Flip(piece);
}

static int HandleEvent(const SDL_Event *e) {
  GridPoint2D *rel = &panel.falling_piece.relative;
  if (e->type == SDL_KEYDOWN) {
    switch (e->key.keysym.sym) {
      case SDLK_DOWN:
        rel->y--;
        if (IsColliding()) {
          rel->y++;
        }
        else {
          last_update_ms = SDL_GetTicks();
        }
        break;
      case SDLK_LEFT:
        rel->x--;
        if (IsColliding()) {
          rel->x++;
        }
        break;
      case SDLK_RIGHT:
        rel->x++;
        if (IsColliding()) {
          rel->x--;
        }
        break;
      case SDLK_UP:
        Flip(&panel.falling_piece);
        if (IsColliding()) {
          FlipBack(&panel.falling_piece);
        }
        break;
    }
  }
  return 0;
}

/**
 * Couleur noire indique que rien ne tombe.
 */
static int IsFalling(void) {
  return !xSDL_ColorEq(&panel.falling_piece.color, &BLACK);
}

static void UpdateNextPiece(void) {
  int piece_num = rand()%NUM_DIFFERENT_PIECES;
  memcpy(panel.next_piece.blocks,
         template[piece_num].fills,
         NUM_PIECE_PARTS * sizeof (GridPoint2D));
  panel.next_piece.color = colors[piece_num];
  int num_flips = rand()%4;
  for (int i = 0; i < num_flips; i++) {
    Flip(&panel.next_piece);
  }
  // Selon le nombre de flips ayant eu lieu, la hauteur devient la largeur.
  if (num_flips % 2 == 1) {
    panel.next_piece.relative = (GridPoint2D) {
      .x = PANEL_COLS/2 - template[piece_num].size.h/2,
      .y = PANEL_ROWS - template[piece_num].size.w/2 - 1
    };
  }
  else {
    panel.next_piece.relative = (GridPoint2D) {
      .x = PANEL_COLS/2 - template[piece_num].size.w/2,
      .y = PANEL_ROWS - template[piece_num].size.h/2 - 1
    };
  }
}

static void SpawnPiece(void) {
  SDL_assert(!IsFalling());
  memcpy(&panel.falling_piece,
         &panel.next_piece,
         sizeof (struct FallingPiece));
  SDL_assert(IsFalling());
  UpdateNextPiece();
}

static void ResetPiece(void) {
  panel.falling_piece.color = BLACK;
  SDL_assert(!IsFalling());
}

static void EmptyLine(int line) {
  /* memset(panel.blocks[line], 0, PANEL_COLS * sizeof (SDL_Color)); */
  for (int j = 0; j < PANEL_COLS; j++) {
    panel.blocks[line][j] = BLACK;
  }
}

static void EliminateLine(int line) {
  for (int i = line+1; i < PANEL_ROWS; i++) {
    memcpy(panel.blocks+i-1,
           panel.blocks+i,
           PANEL_COLS * sizeof (SDL_Color));
  }
  EmptyLine(PANEL_ROWS-1);
}

static int TryScoreLine(int line) {
  for (int i = 0; i < PANEL_COLS; i++) {
    if (xSDL_ColorEq(panel.blocks[line]+i, &BLACK)) {
      return 0;
    }
  }
  EliminateLine(line);
  // 1, 2 ou 3 points selon la hauteur de la ligne.
  return line < 5 ? 1 : (line < 13 ? 2 : 3);
}

static int RefreshPointsText(void) {
  char text[30];
  snprintf(text, sizeof text, "%d", score.points);
  DestroyTextImage(&score.points_text);
  InitTextImage(&score.points_text, GetMediumFont(), text,
    g_rend, &DEFAULT_FG_COLOR);
  score.points_text.pos = (Point2D) {
    .x = PADDING_PX + panel.geom.w - score.points_text.dim.w,
    .y = PADDING_PX
  };
  return 0;
}

static int TryScore(void) {
  int pts = 0;
  int upper_bound = PANEL_ROWS;
  int i = 0;
  int lines = 0;
  while (i < upper_bound) {
    int line_pts = TryScoreLine(i);
    if (line_pts != 0) {
      pts += line_pts;
      upper_bound--;
      lines++;
    }
    else {
      i++;
    }
  }
  if (lines > 0) {
    // Pour chaque ligne supplémentaire retirée les points sont doublés.
    // Si une ligne rapporte X points, en retirer 2 donnera 2X points,
    // en retirer 3 (toujours en une fois) donnera 4X points,  et ainsi de suite..
    pts <<= lines - 1;
    score.points += pts;
    RefreshPointsText();
  }
  return 0;
}

static int Fixate(void) {
  const GridPoint2D *rel = &panel.falling_piece.relative;
  const GridPoint2D *blocks = panel.falling_piece.blocks;

  for (int i = 0; i < NUM_PIECE_PARTS; i++) {
    int x = rel->x + blocks[i].x;
    int y = rel->y + blocks[i].y;
    if (y >= PANEL_ROWS) {
      continue;
    }
    panel.blocks[y][x] = panel.falling_piece.color;
  }
  ResetPiece();
  TryScore();
  return 0;
}

static int Update(void) {
  Uint32 now_ms = SDL_GetTicks();

  if (IsFalling()) {
    Uint32 delta_ms = now_ms - last_update_ms;

    if (delta_ms > FALL_DELAY_MS) {
      GridPoint2D *rel = &panel.falling_piece.relative;

      rel->y--;
      if (IsColliding()) {
        rel->y++;
        Fixate();
      }
      last_update_ms = now_ms;
    }
  }
  else {
    SpawnPiece();
    if (IsColliding()) {
      // Si immédiatement après sa création une pièce collide déjà, 
      // c'est que la partie est terminée : on ajoute le score et on quitte.
      AddScore(score.points);
      ChangeScreen(MENU_SCREEN);
    }
  }
  return 0;
}

static void EmptyBlocks(void) {
  for (int i = 0; i < PANEL_ROWS; i++) {
    EmptyLine(i);
  }
}

static int Focus(void) {
  last_update_ms = SDL_GetTicks();

  EmptyBlocks();
  ResetPiece();
  score.points = 0;
  RefreshPointsText();
  UpdateNextPiece();
  return 0;
}

static int RenderPanelBorder(void) {
  const SDL_Rect border = {
    .x = 0, .y = 0,
    .w = panel.geom.w, .h = panel.geom.h
  };
  xSDL_SetRenderDrawColor(g_rend, &PANEL_BORDER_COLOR);
  SDL_RenderDrawRect(g_rend, &border);
  return 0;
}

static int RenderBlock(const SDL_Rect *rect, const SDL_Color *color) {
  xSDL_SetTextureColorMod(block, color);
  SDL_RenderCopy(g_rend, block, 0, rect);
  return 0;
}

static int RenderPanelBlocks(void) {
  SDL_Rect block_rect = (SDL_Rect) {
    .w = panel.block_dim.w,
    .h = panel.block_dim.h
  };

  for (int i = 0; i < PANEL_ROWS; i++) {
    block_rect.y = (PANEL_ROWS - i - 1)*block_rect.h;
    for (int j = 0; j < PANEL_COLS; j++) {
      block_rect.x = block_rect.w*j;
      RenderBlock(&block_rect, panel.blocks[i] + j);
    }
  }
  xSDL_SetTextureColorMod(block, &WHITE);
  return 0;
}

static int RenderFallingPiece(void) {
  const int block_w = panel.block_dim.w;
  const int block_h = panel.block_dim.h;

  const GridPoint2D *rel = &panel.falling_piece.relative;

  // Rappel, les index verticaux augmentent du bas vers le haut
  const int base_x_px = rel->x*block_w;
  const int base_y_px = (PANEL_ROWS - rel->y - 1)*block_h;

  SDL_Rect block_rect;
  block_rect.w = panel.block_dim.w;
  block_rect.h = panel.block_dim.h;

  for (int i = 0; i < NUM_PIECE_PARTS; i++) {
    const GridPoint2D *block = panel.falling_piece.blocks + i;

    block_rect.x = block->x*block_w + base_x_px;
    block_rect.y = -block->y*block_h + base_y_px;

    RenderBlock(&block_rect, &panel.falling_piece.color);
  }
  xSDL_SetTextureColorMod(block, &WHITE);
  return 0;
}

static int RenderScore(void) {
  RenderTextImage(&score.label_text);
  RenderTextImage(&score.points_text);
  return 0;
}

static int RenderNextPiece(void) {
  SDL_Rect block_rect;
  block_rect.w = panel.block_dim.w;
  block_rect.h = panel.block_dim.h;

  const int base_x = PADDING_PX*2 + panel.geom.w;
  const int base_y = PADDING_PX*2 + MEDIUM_FONT_SIZE +
    block_rect.h*NUM_PIECE_PARTS;

  for (int i = 0; i < NUM_PIECE_PARTS; i++) {
    block_rect.x = base_x + panel.next_piece.blocks[i].x*block_rect.w;
    block_rect.y = base_y - panel.next_piece.blocks[i].y*block_rect.h;
    RenderBlock(&block_rect, &panel.next_piece.color);
  }

  return 0;
}

static int Render(void) {
  SDL_Rect original_viewport;
  SDL_RenderGetViewport(g_rend, &original_viewport);

  SDL_RenderSetViewport(g_rend, &panel.geom);
  RenderPanelBlocks();
  RenderFallingPiece();
  RenderPanelBorder();

  SDL_RenderSetViewport(g_rend, &original_viewport);
  RenderScore();
  RenderNextPiece();

  return 0;
}

int InitGame(SDL_Renderer *g_rend_, const PixelDim2D *screen_dim_) {
  g_rend = g_rend_;
  screen_dim = *screen_dim_;

  /*
   * Disposition horizontale:
   *  - (depuis la gauche) PADDING . PANEL . PADDING . PANEL FOR NEXT PIECE . PADDING
   *
   * Disposition verticale:
   *  - (depuis le haut) PADDING . SCORE . PADDING . PANEL . PADDING
   *
   * Largeur bloc:
   *  - Largeur du panel / PANEL_COLS
   *
   * Hauteur bloc:
   *  - Hauteur du panel / PANEL_ROWS
   *
   * Largeur du panel pour la prochaine pièce:
   *  - 4 * block size
   */

  int panel_top_margin = PADDING_PX*2 + MEDIUM_FONT_SIZE;
  panel.block_dim = (PixelDim2D) {
    .w = (screen_dim.w - 3*PADDING_PX)/(PANEL_COLS+4),
    .h = (screen_dim.h - panel_top_margin - PADDING_PX)/PANEL_ROWS
  };
  panel.geom = (SDL_Rect) {
    .h = panel.block_dim.h*PANEL_ROWS,
    .w = panel.block_dim.w*PANEL_COLS,
    .x = PADDING_PX,
    .y = panel_top_margin
  };

  TTF_Font *font = GetMediumFont();
  InitTextImage(&score.label_text, font, "Pts", g_rend, &DEFAULT_FG_COLOR);
  InitTextImage(&score.points_text, font, "0", g_rend, &DEFAULT_FG_COLOR);

  score.label_text.pos = (Point2D) {.x = PADDING_PX, .y = PADDING_PX};
  score.points_text.pos = (Point2D) {
    .x = PADDING_PX + panel.geom.w - score.points_text.dim.w,
    .y = PADDING_PX
  };

  block = GetTetrisBlockImg();

  const struct ScreenObject self = {
    .Focus = Focus,
    .Render = Render,
    .Update = Update,
    .HandleEvent = HandleEvent,
    .destroy = destroy
  };
  RegisterScreen(GAME_SCREEN, &self);

  return 0;
}
