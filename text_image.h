#ifndef TEXT_IMAGE_H
#define TEXT_IMAGE_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "2D.h"

struct TextImage {
  PixelDim2D dim;
  PixelPoint2D pos;
  SDL_Texture *image;
  SDL_Renderer *rend;
};

int RenderTextImage(struct TextImage *t);

/**
 * Met en place une image de texte. Il subsistera la responsabilité de détruire la texture grâce à
 * SDL_DestroyTexture avant de détruire le renderer.
 *
 * Cela initialisera aussi les valeurs des dimensions. Le réglage de la position revient cependant toujours
 * à l'utilisateur.
 */
int InitTextImage(struct TextImage *ti,
                TTF_Font *font,
                const char *text,
                SDL_Renderer *rend,
                const SDL_Color *color);

int InBounds(const struct TextImage *ti, int x, int y);

void DestroyTextImage(struct TextImage *ti);

#endif
